-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 14, 2016 at 05:53 AM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hotel`
--

-- --------------------------------------------------------

--
-- Table structure for table `add_salary`
--

CREATE TABLE IF NOT EXISTS `add_salary` (
  `id` mediumint(9) unsigned NOT NULL,
  `Period` varchar(20) NOT NULL,
  `Amount` int(11) NOT NULL,
  `paid_date` datetime(1) NOT NULL,
  `id_staff` mediumint(6) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `add_salary`
--

INSERT INTO `add_salary` (`id`, `Period`, `Amount`, `paid_date`, `id_staff`) VALUES
(1, 'bulan', 5000000, '2016-06-04 03:07:08.1', 1),
(2, 'bulan', 0, '2016-07-04 03:07:08.1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `division`
--

CREATE TABLE IF NOT EXISTS `division` (
  `id` char(6) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `desc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `division`
--

INSERT INTO `division` (`id`, `name`, `desc`) VALUES
('D00001', ' Marketing Departemen', 'Markets several hotels to the marketplace according to their needs '),
('D00002', 'Front Office Departemen', 'Sell ??rooms are fully qualified and ready to be occupied by hotel guests .'),
('D00003', 'Housekeeping Departemen ', 'Providing a clean and ready for occupation by hotel guests .'),
('D00004', 'Enggineering & Maintenance Departemen', 'Mengoperasikan, merawat, dan memperbaiki semua peralatan dalam hotel.'),
('D00005', 'Laundry Departemen', 'Membantu departemen Housekeeping dalam menyediakan kebutuhan Linen (Handuk, Seprai, Selimut) untuk kamar hotel dan seragam karyawan.'),
('D00006', 'Food & Beverage Departemen ', 'Menyiapkan makanan dan minuman di dalam hotel.'),
('D00007', 'Finance Departemen ', 'Mengelola keuangan baik pemasukan maupun pengeluaran hotel.'),
('D00008', 'Personnel Departemen ', 'Mengurusi seluruh adsministrasi  karyawan hotel.'),
('D00009', 'Training Departemen ', 'Memberikan berbagai latihan bagi karyawan hotel baik yang baru maupun yang lama.'),
('D00010', 'Security Departemen', 'Menjaga dan mengatur sistem keamanan hotel');

-- --------------------------------------------------------

--
-- Table structure for table `position`
--

CREATE TABLE IF NOT EXISTS `position` (
  `id` mediumint(6) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `salary` int(11) unsigned NOT NULL,
  `id_div` char(6) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `position`
--

INSERT INTO `position` (`id`, `name`, `salary`, `id_div`) VALUES
(1, 'Manager', 7000000, 'D00001'),
(2, 'Admin', 6000000, 'D00007'),
(3, 'Member', 5000000, 'D00003');

-- --------------------------------------------------------

--
-- Stand-in structure for view `semua`
--
CREATE TABLE IF NOT EXISTS `semua` (
`id_staff` mediumint(6) unsigned
,`username` varchar(12)
,`fullname` varchar(50)
,`password` varchar(60)
,`birthdate` date
,`gender` enum('male','female')
,`address` text
,`member_since` date
,`status` bit(1)
,`id_pos` mediumint(6) unsigned
,`position` varchar(50)
,`salary` int(11) unsigned
,`id_div` char(6)
,`Division` varchar(50)
,`desc` text
,`id_add_salary` mediumint(9) unsigned
,`period` varchar(20)
,`amount` int(11)
,`paid_date` datetime(1)
);

-- --------------------------------------------------------

--
-- Table structure for table `semua1`
--

CREATE TABLE IF NOT EXISTS `semua1` (
  `id` mediumint(8) unsigned NOT NULL,
  `id_staff` mediumint(6) unsigned NOT NULL DEFAULT '0',
  `username` varchar(12) NOT NULL,
  `fullname` varchar(50) NOT NULL,
  `password` varchar(60) NOT NULL,
  `birthdate` date NOT NULL,
  `gender` enum('male','female') NOT NULL,
  `address` text NOT NULL,
  `member_since` date NOT NULL,
  `status` bit(1) NOT NULL,
  `id_pos` mediumint(6) unsigned NOT NULL,
  `position` varchar(50) NOT NULL,
  `salary` int(11) unsigned NOT NULL,
  `id_div` char(6) NOT NULL,
  `Division` varchar(50) DEFAULT NULL,
  `desc` text NOT NULL,
  `id_add_salary` mediumint(9) unsigned NOT NULL DEFAULT '0',
  `period` varchar(20) NOT NULL,
  `amount` int(11) NOT NULL,
  `paid_date` datetime(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `semua1`
--

INSERT INTO `semua1` (`id`, `id_staff`, `username`, `fullname`, `password`, `birthdate`, `gender`, `address`, `member_since`, `status`, `id_pos`, `position`, `salary`, `id_div`, `Division`, `desc`, `id_add_salary`, `period`, `amount`, `paid_date`) VALUES
(1, 1, 'hetlysaint', 'Hetly Saint Kartika', 'saint002', '1996-06-24', 'female', 'jalan setia budi pasar 1 medan', '2008-05-04', b'0', 1, 'Manager', 7000000, 'D00001', ' Marketing Departemen', 'Markets several hotels to the marketplace according to their needs ', 1, 'bulan', 5000000, '2016-06-04 03:07:08.1'),
(2, 1, 'hetlysaint', 'Hetly Saint Kartika', 'saint002', '1996-06-24', 'female', 'jalan setia budi pasar 1 medan', '2008-05-04', b'0', 1, 'Manager', 7000000, 'D00001', ' Marketing Departemen', 'Markets several hotels to the marketplace according to their needs ', 2, 'bulan', 0, '2016-07-04 03:07:08.1');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE IF NOT EXISTS `staff` (
  `id` mediumint(6) unsigned NOT NULL,
  `username` varchar(12) NOT NULL,
  `fullname` varchar(50) NOT NULL,
  `password` varchar(60) NOT NULL,
  `birthdate` date NOT NULL,
  `gender` enum('male','female') NOT NULL,
  `address` text NOT NULL,
  `member_since` date NOT NULL,
  `forget_key` char(5) NOT NULL,
  `status` bit(1) NOT NULL,
  `id_pos` mediumint(6) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`id`, `username`, `fullname`, `password`, `birthdate`, `gender`, `address`, `member_since`, `forget_key`, `status`, `id_pos`) VALUES
(1, 'hetlysaint', 'Hetly Saint Kartika', 'saint002', '1996-06-24', 'female', 'jalan setia budi pasar 1 medan', '2008-05-04', '0', b'0', 1),
(2, 'Wendys', 'Wendy Winata', 'winata001', '1996-01-11', 'male', 'jalan dr mansur no.2a', '2009-09-07', '0', b'1', 2),
(3, 'aggieee', 'Aggie Wicita', 'wicita', '1996-10-09', 'female', 'jalan pembangunan no 8a medan', '2007-09-08', '0', b'0', 3),
(4, 'samuelezzay', 'Ezzay Tarigan', 'ezzay09', '1995-10-05', 'male', 'jalan berdikari no.99', '2008-01-01', '0', b'0', 1);

-- --------------------------------------------------------

--
-- Structure for view `semua`
--
DROP TABLE IF EXISTS `semua`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `semua` AS select `a`.`id` AS `id_staff`,`a`.`username` AS `username`,`a`.`fullname` AS `fullname`,`a`.`password` AS `password`,`a`.`birthdate` AS `birthdate`,`a`.`gender` AS `gender`,`a`.`address` AS `address`,`a`.`member_since` AS `member_since`,`a`.`status` AS `status`,`a`.`id_pos` AS `id_pos`,`b`.`name` AS `position`,`b`.`salary` AS `salary`,`b`.`id_div` AS `id_div`,`c`.`name` AS `Division`,`c`.`desc` AS `desc`,`d`.`id` AS `id_add_salary`,`d`.`Period` AS `period`,`d`.`Amount` AS `amount`,`d`.`paid_date` AS `paid_date` from (((`staff` `a` join `position` `b`) join `division` `c`) join `add_salary` `d`) where ((`a`.`id_pos` = `b`.`id`) and (`b`.`id_div` = `c`.`id`) and (`d`.`id_staff` = `a`.`id`));

--
-- Indexes for dumped tables
--

--
-- Indexes for table `add_salary`
--
ALTER TABLE `add_salary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `division`
--
ALTER TABLE `division`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `position`
--
ALTER TABLE `position`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `semua1`
--
ALTER TABLE `semua1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `add_salary`
--
ALTER TABLE `add_salary`
  MODIFY `id` mediumint(9) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `position`
--
ALTER TABLE `position`
  MODIFY `id` mediumint(6) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `semua1`
--
ALTER TABLE `semua1`
  MODIFY `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `id` mediumint(6) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

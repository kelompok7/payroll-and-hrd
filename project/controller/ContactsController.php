<?php

require_once 'model/ContactsService.php';

class ContactsController {
    
    private $contactsService = NULL;
    
    public function __construct() {
        $this->contactsService = new ContactsService();
    }
    
    public function redirect($location) {
        header('Location: '.$location);
    }
    
    public function handleRequest() {
        $op = isset($_GET['op'])?$_GET['op']:NULL;
        try {
            if ( !$op || $op == 'login' ) {
                $this->login();
            } elseif ( $op == 'new' ) {
                $this->saveContact();
            }elseif ( $op == 'list' ) {
                $this->listContacts();
            } elseif ( $op == 'delete' ) {
                $this->deleteContact();
            } elseif ( $op == 'show' ) {
                $this->showStaff();
            } else {
                $this->showError("Page not found", "Page for operation ".$op." was not found!");
            }
        } catch ( Exception $e ) {
            // some unknown Exception got through here, use application error page to display it
            $this->showError("Application error", $e->getMessage());
        }
    }
    
    public function listContacts() {
       $staffs = $this->contactsService->getAllContacts();
        include 'view/staff.php';
    }
	public function login() {
        $username = isset($_POST['username'])?$_POST['username']:NULL;
		 $password = isset($_POST['password'])?$_POST['password']:NULL;
        $contacts = $this->contactsService->loginStaff($username,$password);
        include 'view/login.php';
	}
    
    public function saveContact() {
       
        $title = 'Add new staff';
        
        $username = '';
        $fullname = '';
        $password = '';
        $birthdate = '';
		$gender = '';
        $address = '';
        $member_since = '';
		$forget_key=0;
		$status=1;
		$id_pos=0;
       
        $errors = array();
        
        if ( isset($_POST['form-submitted']) ) {
            
            $username       = isset($_POST['username']) ?   $_POST['username']  :NULL;
            $fullname      = isset($_POST['fullname'])?   $_POST['fullname'] :NULL;
            $password      = isset($_POST['password'])?   $_POST['password'] :NULL;
			$birthdate       = isset($_POST['birthdate']) ?   $_POST['birthdate']  :NULL;
            $gender      = isset($_POST['gender'])?   $_POST['gender'] :NULL;
            $address    = isset($_POST['address'])? $_POST['address']:NULL;
			$member_since      = isset($_POST['member_since'])?   $_POST['member_since'] :NULL;
			$forget_key       = isset($_POST['forget_key']) ?   $_POST['forget_key']  :NULL;
            $status      = isset($_POST['status'])?   $_POST['status'] :NULL;
            $id_pos    = isset($_POST['id_pos'])? $_POST['id_pos']:NULL;
            
            try {
                $this->contactsService->createNewContact($username, $fullname, $password, $birthdate,$gender,$address,$member_since,$forget_key,$status,$id_pos);
                $this->redirect('index.php');
                return;
            } catch (ValidationException $e) {
                $errors = $e->getErrors();
            }
        }
        
        include 'view/contact-form.php';
    }
    
    public function deleteContact() {
        $id = isset($_GET['id'])?$_GET['id']:NULL;
        if ( !$id ) {
            throw new Exception('Internal error.');
        }
        
        $this->contactsService->deleteContact($id);
        
        $this->redirect('index.php');
    }
    
    public function showStaff() {
        $id = isset($_GET['id'])?$_GET['id']:NULL;
        if ( !$id ) {
            throw new Exception('Internal error.');
        }
        $staff = $this->contactsService->getStaff($id);
        
        include 'view/profil_pegawai.php';
    }
	 public function showProfil() {
        $id = isset($_GET['username'])?$_GET['password']:NULL;
        if ( !$id ) {
            throw new Exception('Internal error.');
        }
        $contact = $this->contactsService->getContact($id);
        
        include 'view/profil_pegawai.php';
    }
    
    public function showError($title, $message) {
        include 'view/error.php';
    }
    
}
?>
